import math
import time
from collections import namedtuple
from copy import copy
import ev3_dc as ev3

from plan_execute.utils import get_direction_error, clip


def motor_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    Vehicle = ev3.TwoWheelVehicle(radius_wheel=0.0275, tread=0.159, protocol=ev3.USB)
    # arm = ev3.Motor(port=ev3.PORT_C, ev3_obj=Vehicle)  # -40 nice resting, -95 for 1 disc +5 for every collected disc
    # arm.move_to(-10, speed=5, brake=False).start()
    position_old = namedtuple('VehiclePosition', ['x', 'y', 'o'])
    position_old.x = 0
    position_old.y = 0
    position_old.o = 0

    position_old_e = namedtuple('VehiclePosition', ['x', 'y', 'o'])
    position_old_e.x = 0
    position_old_e.y = 0
    position_old_e.o = 0
    while True:
        name_r, (cmd, data) = remote_a.recv()
        # print(name_r, (cmd, data))
        if cmd == 'set_motor':
            (speed, turn) = data
            turn = clip(-180, 180, turn)  # valid ranges for Vehicle.move
            speed = clip(-100, 100, speed)  # valid ranges for Vehicle.move
            Vehicle.move(int(speed), int(turn))
        elif cmd == 'move_arm':
            position = data
            # arm.move_to(position, speed=5, brake=False).start()
        elif cmd == "print":
            print(Vehicle.position)
        elif cmd == 'get_motor_data':
            '''position = copy(Vehicle.position)
            remote_a.send((name_r, [position.x - position_old.x,
                                    position.y - position_old.y,
                                    get_direction_error(position.o*2*math.pi/360, position_old.o*2*math.pi/360)]))
            position_old = copy(position)'''

            position = Vehicle.position
            remote_a.send((name_r, [
                math.sqrt(math.pow(position.x - position_old.x, 2) + math.pow(position.y - position_old.y, 2)),
                get_direction_error(position.o * 2 * math.pi / 360, position_old.o * 2 * math.pi / 360)]))
            position_old = position
        elif cmd == 'reset_position_execute':
            position_old_e = Vehicle.position
        elif cmd == 'get_position_execute':
            position = Vehicle.position
            remote_a.send((name_r, [position.x - position_old_e.x, position.y - position_old_e.y,
                                    get_direction_error(position.o * 2 * math.pi / 360, position_old_e.o * 2 * math.pi / 360)]))
            position_old_e = position

        elif cmd == 'stop':
            Vehicle.stop(brake=False)
        elif cmd == 'close':
            remote_a.close()
        else:
            print(cmd, data)
            raise NotImplementedError
