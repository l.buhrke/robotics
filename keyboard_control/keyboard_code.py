def keyboard_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    import curses
    speed, turn = 0, 0
    stdscr = curses.initscr()
    curses.cbreak()
    stdscr.addstr(0, 0, 'use Arrows to navigate your vehicle')
    stdscr.addstr(1, 0, 'pause your vehicle with key <p>')
    stdscr.addstr(2, 0, 'terminate with key <q>')
    debug = None
    while True:
        stdscr.addstr(4, 0, f'speed: {speed:4d}, turn: {turn:4d}, debug: {debug}')
        c = stdscr.getch()
        if c == ord('w'):
            speed = min(speed + 5, 80)
            remote_r.send(("motor_control", ("set_motor", (speed, turn))))
        elif c == ord('p'):
            remote_r.send(("motor_control", ("print", None)))
        elif c == ord('x'):
            remote_r.send(("motor_control", ("stop", None)))
            speed, turn = 0, 0
        elif c == ord('s'):
            speed = max(speed - 5, -80)
            remote_r.send(("motor_control", ("set_motor", (speed, turn))))
        elif c == ord('a'):
            turn = min(turn + 20, 180)
            remote_r.send(("motor_control", ("set_motor", (speed, turn))))
        elif c == ord('d'):
            turn = max(turn - 20, -180)
            remote_r.send(("motor_control", ("set_motor", (speed, turn))))