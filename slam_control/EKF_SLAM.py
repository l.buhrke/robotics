import time
from dataclasses import dataclass, field
from typing import List
import numpy as np
from planning.utils import l2
from slam_control.xy_slam import EKF_SLAMxy


dy = -0.205  # hight of the camera !!! replace later with precise data !!!
dx = -0.05
theta = -(41 / 180) * np.pi  # x-axis rotation of the camera !!! replace later with precise estimation !!!
#19
### matrix for camera coordinate to world coordinate transformation
camToWorld = np.array([[1, 0, 0, 0], [0, np.cos(theta), -np.sin(theta), dy], [0, np.sin(theta), np.cos(theta), dx], [0, 0, 0, 1]])

def camera_detections(robot_pose, theta, tvec, id):
    ''' get landmarks from camera image and return a list
        where each item is a landmark observation:
        [[actual_measurement, world_coords, camera_coords, identity], ...] '''
    # tvec, id = get_camera_feed() # (return tvec and id)
    detection = []
    c, s = np.cos(theta), np.sin(theta)
    rot_m = np.array(((c, -s), (s, c)))
    for i, vec in enumerate(tvec):
        vec = np.resize(vec, (4))
        vec[3] = 1.0

        vec = camToWorld @ vec
        #print(vec)
        vec_n = np.array((vec[2], -vec[0]))
        vec_world_trans = rot_m @ vec_n

        r = np.sqrt(vec[0] ** 2 + vec[2] ** 2)
        alpha = np.arctan2(-vec[0], vec[2])

        world_coords = [robot_pose[0] + vec_world_trans[0], robot_pose[1] + vec_world_trans[1]]
        actual_measurement = [r, alpha]

        if 1 < id[i] < 600:
            detection.append((actual_measurement, world_coords, vec, int(id[i])))

    return detection


@dataclass
class Marker:
    position: List = field(default_factory=lambda: [])
    dcount: int = 0


class EKF_SLAM:
    ''' Class for implementation of the EKF_SLAM algorithm'''

    n = 100
    mu = np.zeros((3 + 2 * 100, 1), dtype=np.float32)
    id_list = {}
    inf = 1e12
    Sigma = inf * np.eye(2 * n + 3)
    Sigma[:3, :3] = np.zeros((3, 3))
    # S = np.zeros((5, 5), dtype=np.float32)
    Q = np.zeros((2, 2), dtype=np.float32)

    H = np.zeros((2, 3 + 2 * n), dtype=np.float32)

    I = np.identity(3 + 2 * n, dtype=np.float32)

    G = np.identity(3, dtype=np.float32)
    V = np.zeros((3, 2), dtype=np.float32)

    mu_rel = np.zeros((5, 1), dtype=np.float32)

    def __init__(self, initial_state):
        self.mu[0] = initial_state[0]
        self.mu[1] = initial_state[1]
        self.mu[2] = initial_state[2]
        # self.Sigma = initial_covariance
        self.mu_index = 3
        self.id_index = 0

        self.pre_add_dict: dict[int, Marker] = {}  # id: position, count
        self.pre_add_count = 3
        self.pre_add_eps = 0.1

        self.Qt = np.array([[0.1, 0],
                            [0, 0.02]])

    def predict(self, movements):
        ''' return Position of the robot through l and r'''
        [dx, dy, da] = movements
        n = len(self.mu)
        if dx == 0 and dy == 0 and da == 0:
            return
        motion = np.array([[dx],
                           [dy],
                           [da]])
        F = np.append(np.eye(3), np.zeros((3, n - 3)), axis=1)
        mu_bar = self.mu + F.T.dot(motion)

        G = np.eye(n)
        Rt = 6 * np.array([[abs(dx) * 0.1, 0, 0],
                       [0, abs(dy) * 0.1, 0],
                       [0, 0, abs(da) * 0.05]])

        cov_bar = G.dot(self.Sigma).dot(G.T) + F.T.dot(Rt).dot(F)
        self.Sigma = cov_bar
        self.mu = mu_bar

    def add_landmark(self, world_coords, identity):
        if identity not in self.pre_add_dict:
            marker = Marker(position=world_coords, dcount=1)
            self.pre_add_dict[identity] = marker
            return 0

        if l2(world_coords, self.pre_add_dict[identity].position) < self.pre_add_eps:
            self.pre_add_dict[identity].dcount += 1
        else:
            self.pre_add_dict[identity].dcount = 1
            self.pre_add_dict[identity].position = world_coords

        if self.pre_add_dict[identity].dcount < self.pre_add_count:
            return 0

        ''' add landmark to the coordinate list mu and the errors to the covariance matrix sigma '''
        # add markers to mu
        self.mu[self.mu_index] = world_coords[0]
        self.mu[self.mu_index + 1] = world_coords[1]
        self.id_list[identity] = self.id_index

        # set index up
        self.mu_index += 2
        self.id_index += 1
        return 1

    def correction(self, actual_measurement, identity, world_coord=None):
        if not (identity in self.id_list):
            if 0 == self.add_landmark(world_coord, identity):
                return

        self.pre_add_dict[identity].dcount += 1

        N = len(self.mu)
        j = self.id_list[identity]
        [r, theta] = actual_measurement

        # compute expected observation
        dx = np.array([self.mu[2 * j + 3][0] - self.mu[0][0]])
        dy = np.array([self.mu[2 * j + 4][0] - self.mu[1][0]])
        sq = np.sqrt(np.power(dx, 2) + np.power(dy, 2))
        h_theta = np.arctan2(dy, dx)

        h = np.array([sq, h_theta - self.mu[2][0]])

        H = np.zeros((2, N))

        H[0, 0] = -dx / sq
        H[0, 1] = -dy / sq
        H[0, 2] = 0
        H[1, 0] = dy / np.power(sq, 2)
        H[1, 1] = -dx / np.power(sq, 2)
        H[1, 2] = -1
        H[0, (3 + 2 * j)] = -H[0, 0]
        H[0, (4 + 2 * j)] = -H[0, 1]
        H[1, (3 + 2 * j)] = -H[1, 0]
        H[1, (4 + 2 * j)] = -H[1, 1]

        K = self.Sigma.dot(H.T).dot(np.linalg.inv(H.dot(self.Sigma).dot(H.T) + self.Qt))

        h_dif = np.array([[r], [theta]]) - h
        h_dif = (h_dif + np.pi) % (2 * np.pi) - np.pi

        self.mu = self.mu + K.dot(h_dif)
        self.Sigma = (np.eye(N) - K.dot(H)).dot(self.Sigma)

    @staticmethod
    def compute_eig_metric(small_sigma):
        w, v = np.linalg.eig(small_sigma)
        if w[1] > w[0]:
            dummy_v = v[0]
            dummy_w = w[0]
            v[0] = v[1]
            v[1] = dummy_v
            w[0] = w[1]
            w[1] = dummy_w
        v[0] = np.transpose(v[0])
        return [np.sqrt(w[0]), np.sqrt(w[1]), np.arctan2(v[0, 0], v[0, 1])]

    def get_robot_pose(self):
        ''' read out the robot position and angle from mu variable
            read out robot error from Sigma '''

        eig_metric = self.compute_eig_metric(self.Sigma[0:2, 0:2])
        self.mu[2][0] = self.mu[2][0] % (2 * np.pi)
        errors = np.diagonal(self.Sigma[0:3, 0:3])

        robot_pose_dict = {'position': (self.mu[:2, 0], eig_metric), 'theta': (self.mu[2, 0], errors[2])}
        return robot_pose_dict

    def solve_position_conflict(self, position, landmark_data_dict, landmark_id):
        landmark_data_dict_copy = landmark_data_dict.copy()
        for k_identiy, (k_position, _) in landmark_data_dict.items():
            if l2(position, k_position) < 0:  # conflict
                if self.pre_add_dict[landmark_id].dcount > self.pre_add_dict[k_identiy].dcount:  # kick out old one
                    landmark_data_dict_copy.pop(k_identiy)
                else:
                    return 1, landmark_data_dict_copy  # reject the new one
        return 0, landmark_data_dict_copy  # no conflict

    def get_landmark_positions(self):
        ''' read out the landmark positions from mu variable
            read out landmark error from Sigma '''

        landmark_data_dict = {}
        for (landmark_id, index) in self.id_list.items():
            position = [self.mu[3 + 2 * index][0], self.mu[3 + 2 * index + 1][0]]
            skip_new_one, landmark_data_dict = self.solve_position_conflict(position=position,
                                                                            landmark_data_dict=landmark_data_dict,
                                                                            landmark_id=landmark_id)
            if 1 == skip_new_one:
                continue

            eig_metric = self.compute_eig_metric(self.Sigma[(3 + index):(3 + index + 2), (3 + index):(3 + index + 2)])
            landmark_data_dict[landmark_id] = (position, eig_metric)

        return landmark_data_dict, self.Sigma[:self.mu_index, :self.mu_index]  # , sigma


def slam_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    # print("slam ps started")

    # your arbitrary start position
    initial_state = (0, 0, 0)
    slamxy = EKF_SLAMxy(initial_state)

    iterations = 0
    start_time = time.time() - 1e-5
    landmark_observations = None
    while True:
        iterations += 1
        #print("slam_fps: ", iterations/(time.time()-start_time))

        if remote_a.poll(0):  # if remote empty skip this
            name_r, (cmd, data) = remote_a.recv()
            if cmd == 'get_position':
                remote_a.send((name_r, slamxy.get_robot_pose()))
            elif cmd == 'get_slam_data':
                remote_a.send((name_r, (slamxy.get_robot_pose(), slamxy.get_landmark_positions())))
            elif cmd == 'slam_publish':
                remote_a.send((name_r, (slamxy.get_robot_pose(), slamxy.get_landmark_positions(), landmark_observations)))
            elif cmd == 'close':
                remote_a.close()
                break
            else:
                print(name_r, (cmd, data))
                raise NotImplementedError
        # slam stuff #
        remote_r.send(("motor_control", ("get_motor_data", None)))  # get motor and camera data
        movements = remote_r.recv()
        slamxy.predict(movements)
        remote_r.send(("camera_control", ("get_camera_data", None)))
        ids, tvec = remote_r.recv()
        robot_data = slamxy.get_robot_pose()
        landmark_observations = camera_detections(robot_pose=robot_data['position'][0], theta=robot_data['theta'][0], tvec=tvec, id=ids)

        for i, landmark_observation in enumerate(landmark_observations):
            actual_measurement, world_coords, camera_coords, identity = landmark_observation
            slamxy.correction(actual_measurement, identity, world_coord=world_coords)


if __name__ == '__main__':
    pass

