import math
import time

import numpy as np

from slam_control.EKF_SLAM import EKF_SLAM


def slam_control_dummy(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    print("slam ps started")
    measurement_x_stddev = 0.5  # x error from measurement
    measurement_y_stddev = 0.5  # y error from measurement

    measurement_distance_stddev = 1.4 * np.sqrt(0.1)  # radial distance error
    measurement_angle_stddev = (5 / 180) * np.pi  # 5 degree error

    predict_l_stddev = 0.03  # 0.07  # uncertainty of the predicted l route
    predict_r_stddev = 0.03  # 0.07  # uncertainty of the predicted r route

    # your arbitrary start position
    initial_state = (0, 0, 0)
    initial_covariance = np.zeros((3, 3), dtype=np.float32)
    slam = EKF_SLAM(initial_state, initial_covariance, measurement_distance_stddev, measurement_angle_stddev,
                    predict_l_stddev, predict_r_stddev, measurement_x_stddev, measurement_y_stddev)
    slam.add_landmark([1, 1], 100)
    iterations = 0
    start_time = time.time() - 1e-5
    landmark_observations = None
    while True:
        iterations += 1
        # print("slam_fps: ", iterations/(time.time()-start_time))

        if remote_a.poll(0):  # if remote empty skip this
            # basic communication #
            name_r, (cmd, data) = remote_a.recv()
            if cmd == 'get_position':
                remote_a.send((name_r, slam.get_robot_pose()))
            elif cmd == 'get_slam_data':
                remote_a.send((name_r, (slam.get_robot_pose(), slam.get_landmark_positions())))
            elif cmd == 'slam_publish':
                remote_a.send((name_r, (slam.get_robot_pose(), slam.get_landmark_positions(), landmark_observations)))
            elif cmd == 'close':
                remote_a.close()
                break
            else:
                print(name_r, (cmd, data))
                raise NotImplementedError

        a = math.atan2(1, 1) # + random noise
        r = math.sqrt(2) # + random noise
        slam.correction(np.array([r, a]), 100)

        #print("landmark estimated positions:", slam.get_landmark_positions())
