import math
import time
from plan_execute.utils import CPID, clip, get_direction_error, get_target_dir, l2


class CPath_follower:
    def __init__(self):
        self.path = None
        self.target = None
        self.target_type = None
        self.epsilon = 0.1

    def load(self, path, target_type):
        self.path = path
        self.target_type = target_type
        if len(self.path) > 0:  # TODO find best starting point not just the first of the list maybe idk
            self.target = self.path.pop(0)
        else:
            self.target = None

    def step(self, robot_position):  # return target_pos and if path is done
        reached = False
        if self.target is None:
            return robot_position, True

        while l2(self.target, robot_position) < self.epsilon:
            if len(self.path) > 0:
                self.target = self.path.pop(0)
            else:  # path is completed
                return robot_position, True
        return self.target, reached


def plan_execute_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    path = []
    PID = CPID()
    Path_Follower = CPath_follower()
    Path_Follower.load(path, 'race')
    racing = False
    switch_tick = 4
    switch_start = time.time()
    plan_idle = 0
    plan_max = 5
    ident = -10
    while True:
        remote_r.send(('motor_control', ('stop', None)))
        while plan_idle < plan_max:
            if remote_a.poll(0):
                name_r, (cmd, data) = remote_a.recv()
                if cmd == "new_path":
                    path, ident, racing = data
                    Path_Follower.load(path, 'race')
                    plan_idle += 1
                elif cmd == 'get_path':
                    remote_a.send((name_r, [path]))
                else:
                    print(cmd, data)
                    raise NotImplementedError

        remote_r.send(("slam_control", ("get_position", None)))
        robot_data = remote_r.recv()
        robot_position, _ = robot_data['position']
        robot_dir, _ = robot_data['theta']

        remote_r.send(("motor_control", ("reset_position_execute", None)))

        switch_start = time.time()
        plan_idle = 0
        print("follow path", path)
        print("id: ", ident)
        while (time.time() - switch_start) < switch_tick:
            remote_r.send(("motor_control", ("get_position_execute", None)))
            [dx, dy, da] = remote_r.recv()

            robot_position[0] += dx
            robot_position[1] += dy
            robot_dir += da
            robot_dir = robot_dir % (2*math.pi)

            target, done = Path_Follower.step(robot_position)
            if done:
                if racing:
                    remote_r.send(('motor_control', ('set_motor', (20, 0))))
                    time.sleep(1)
                    remote_r.send(('motor_control', ('stop', None)))
                    print("FINISHED!!!")
                    break
                else:
                    remote_r.send(('motor_control', ('stop', None)))
                continue
            target_dir = get_target_dir(target_pos=target, player_pos=robot_position)
            direction_error = get_direction_error(target_dir=target_dir, robot_dir=robot_dir)

            turn = PID.step(direction_error)
            if racing:
                speed = clip(30, 50, 20 * l2(robot_position, target) / (abs(direction_error) + 1e-5))
            else:
                speed = clip(5, 10, 20 * l2(robot_position, target) / (abs(direction_error) + 1e-5))
            remote_r.send(('motor_control', ('set_motor', (speed, turn))))  # pos turn -> links

            # TODO follow path or pick_up chip
