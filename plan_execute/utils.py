import math

import numpy as np


def get_direction_error(target_dir, robot_dir):
    robot_dir = robot_dir % (2*math.pi)
    target_dir = target_dir % (2 * math.pi)

    if target_dir > robot_dir:
        links = target_dir - robot_dir
        rechts = robot_dir + 2*math.pi-target_dir
    else:
        links = target_dir + 2*math.pi-robot_dir
        rechts = robot_dir - target_dir
    if links < rechts:
        return links  # pos
    else:
        return -rechts


def get_target_dir(target_pos, player_pos):
    dx = target_pos[0] - player_pos[0]
    dy = target_pos[1] - player_pos[1]

    return atan2to2pi(math.atan2(dy, dx))


class CPID:
    def __init__(self, kp=150/(math.pi/2), ki=0, kd=0):  # TODO finetuning see https://en.wikipedia.org/wiki/PID_controller manual tuning
        self.k_p = kp
        self.k_i = ki
        self.k_d = kd
        self.sum_err = 0
        self.last_error = None

    def reset(self):
        self.sum_err = 0
        self.last_error = None

    def step(self, error):
        self.sum_err += error
        if self.last_error is None:
            self.last_error = error
            change = 0
        else:
            change = error - self.last_error
            self.last_error = error

        return self.k_p*error + self.k_i*self.sum_err + self.k_d*change


def clip(minv, maxv, value):
    return max(minv, min(maxv, value))


def l2(p1, p2):
    return math.sqrt(math.pow(p1[0] - p2[0], 2) + math.pow(p1[1] - p2[1], 2))


def atan2to2pi(alpha):
    if 0 < alpha <= math.pi:
        return alpha
    return 2*math.pi + alpha


def twopi_to_atan2(pi):
    pi = pi % (2 * np.pi)
    if pi >= np.pi:
        return pi - 2 * np.pi
    return pi


if __name__ == '__main__':
    pass