import time

from plan_execute.plan_execute_code import CPath_follower
from plan_execute.utils import CPID


def plan_execute_control_dummy(remote_r, parent_remote_r, remote_a, parent_remote_a):
    path = []
    PID = CPID()
    Path_Follower = CPath_follower()
    #path = [[1.1, 0], [1.1, 1], [0, 0], [0, 1], [1.1, 1], [0.15, 0.15]]#  [[1.2, 0.1], [0.8, 0.2], [1.2, 0.5], [0.5, 1], [0.4, 0.8], [0.8, 0.5], [0, 0]]  # 0.3m in front, 1m to the left y pos left
    Path_Follower.load(path, 'race')
    r, l = (0, 0)
    not_cycling = time.time()
    max_cycling_time = 10
    while True:
        target_reached = False
        if remote_a.poll(0):
            name_r, (cmd, data) = remote_a.recv()
            if cmd == "new_path":
                path = data
            elif cmd == 'reaction':
                [r, l] = data
                #remote_r.send(('motor_control', ('set_motor', (10, int(15*(l-r))))))  # pos turn -> links
            elif cmd == 'get_path':
                remote_a.send((name_r, path))
            else:
                print(cmd, data)
                raise NotImplementedError