from camera_control.camera_code import camera_control
from camera_control.camera_dummy_code import camera_control_dummy

from motor_control.motor_code import motor_control
from motor_control.motor_dummy_code import motor_control_dummy

from slam_control.EKF_SLAM import slam_control
from slam_control.slam_dummy_code import slam_control_dummy

from viewer_subscriber_publisher.publish_code import publish_control
from keyboard_control.keyboard_code import keyboard_control

from plan_execute.plan_execute_code import plan_execute_control
from plan_execute.plan_execute_dummy_code import plan_execute_control_dummy

from planning.planning import planning_control
from planning.planning_dummy_code import planning_dummy

from multiprocessing import Process, Pipe


class CBrain:
    def __init__(self, remote_dict):
        self.remote_dict = remote_dict
        self.start()

    def start(self):
        while True:
            for controller, (remote_r, remote_a) in self.remote_dict.items():
                if remote_r.poll(0):  # if remote empty skip this
                    answer = remote_r.recv()
                    try:
                        name_a, (cmd, data) = answer
                    except ValueError as e:
                        print(answer)
                        print(controller)
                        print(e)
                        raise ValueError

                    if name_a in self.remote_dict:
                        self.remote_dict[name_a][1].send((controller, (cmd, data)))
                    else:
                        print(cmd, data)
                        print("Invalid from remote: ", controller)
                        raise NotImplementedError
                if remote_a.poll(0):
                    name_r, data = remote_a.recv()

                    if name_r in self.remote_dict:
                        self.remote_dict[name_r][0].send(data)
                    else:
                        print(name_r, data)
                        print("Invalid from remote: ", controller)
                        raise NotImplementedError


if __name__ == '__main__':
    remote_dict = {"camera_control": camera_control,
                   "motor_control": motor_control,
                   "slam_control": slam_control,
                   #"keyboard_control": keyboard_control,
                   "planning_control": planning_control,
                   "plan_execute_control": plan_execute_control,
                   "publish_control": publish_control
                   }
    ps_list = []
    for key in remote_dict.keys():
        (remote_r, work_remote_r) = Pipe()
        (remote_a, work_remote_a) = Pipe()
        ps = Process(target=remote_dict[key], args=(work_remote_r, remote_r, work_remote_a, remote_a))
        ps.daemon = False
        ps.start()
        work_remote_r.close()
        work_remote_a.close()
        remote_dict[key] = (remote_r, remote_a)

    Brain = CBrain(remote_dict)