import time
import jsonpickle
from camera_control.ImageProcessing import imagePro
from lib.message import Message
from viewer_subscriber_publisher.publisher import Publisher


def publish_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    publisher = Publisher()
    i = 0

    while True:
        #remote_r.send(("planning_control", ("get_image", None)))
        remote_r.send(("camera_control", ("get_image", None)))
        image = remote_r.recv()
        image = imagePro(image)
        remote_r.send(('planning_control', ('get_path_and_connections', None)))
        #remote_r.send(('plan_execute_control', ('get_path', None)))
        path = remote_r.recv()
        remote_r.send(('slam_control', ('slam_publish', None)))
        robot_data, (landmark_data, sigma), landmark_obs = remote_r.recv()
        robot_position, robot_position_err = robot_data['position']
        robot_theta, theta_err = robot_data['theta']
        msg = Message()

        msg.id = i
        msg.timestamp = time.time()
        msg.start = False

        '''if sigma.size > 0:
            minimum = np.amin(sigma)
            maximum = np.amax(sigma)
            matrix_zeros = np.zeros(sigma.shape, dtype=np.uint8)
            matrix_zeros = np.expand_dims(matrix_zeros, -1)
            matrix_img = cv2.cvtColor(matrix_zeros, cv2.COLOR_GRAY2BGR)
            if maximum > 0:
                matrix_img[sigma > 0, 0] = sigma[sigma > 0] * 255 / maximum
            if minimum < 0:
                matrix_img[sigma < 0, 2] = sigma[sigma < 0] * 255 / minimum
            matrix_img = cv2.resize(matrix_img, (400, 400), interpolation=cv2.INTER_AREA)
            image[0:400, 0:400] = matrix_img'''


        # landmark measurements
        try:
            for node_id, tpos in landmark_data.items():
                pos, err = tpos
                msg.landmark_estimated_positions.append(pos)  # shape (n, 2)
                msg.landmark_estimated_ids.append(node_id)  # shape (n,)
                msg.landmark_estimated_stdevs.append(err)  # shape (n, 2)


            for _, landmark_observation in enumerate(landmark_obs):
                actual_measurement, world_coords, camera_coords, identity = landmark_observation
                msg.landmark_ids.append(identity)  # shape (m,)
                msg.landmark_rs.append(actual_measurement[0])  # shape(m,)
                msg.landmark_alphas.append(actual_measurement[1])  # shape(m,)
                msg.landmark_positions.append(world_coords)  # shape (m, 2)
        except:
            pass

        msg.robot_position = robot_position  # shape (2,)
        msg.robot_theta = robot_theta
        msg.robot_stdev = robot_position_err  # shape (3,)
        msg.path = path

        msg_str = jsonpickle.encode(msg)
        publisher.publish_img(msg_str, image)
        i += 1