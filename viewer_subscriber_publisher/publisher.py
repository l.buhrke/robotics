"""publisher.py -- send PiCamera image stream, compressing images.

publisher
"""

import sys

import socket
import time
import traceback
import cv2
import imagezmq
import jsonpickle
import subprocess
import numpy as np
from time import sleep
from datetime import datetime
from lib.message import Message
from PIL import Image
from lib.utiles import ARUCO_DICT
#import ev3_dc as ev3
import argparse
from lib.CountsPerSec import CountsPerSec

class Publisher():
    def __init__(self) -> None: 
        # use either of the formats below to specifiy address of display computer
        # sender = imagezmq.ImageSender(connect_to='tcp://100.76.2.68:5555')

        # PUB/SUB:
        self.sender = imagezmq.ImageSender(connect_to='tcp://*:5555', REQ_REP=False)

        self.jpeg_quality = 15  # 0 to 100, higher is better quality, 95 is cv2 default

    @staticmethod
    def print_on_image(image):

        curr_time = datetime.now()
        formatted_time = curr_time.strftime('%H:%M:%S.%f')

        text = "milliseconds: " + str(formatted_time)
        org = (30, 60)
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontScale = 1
        color = (0,0,255)  #(B, G, R)
        thickness = 2
        lineType = cv2.LINE_AA
        bottomLeftOrigin = False

        img_text = cv2.putText(image, text, org, font, fontScale, color, thickness, lineType, bottomLeftOrigin)

        return image

    def publish_img(self, msg, image):

        # image = self.print_on_image(image)
        # processing of image before sending would go here.
        # for example, rotation, ROI selection, conversion to grayscale, etc.
        ret_code, jpg_buffer = cv2.imencode(
            ".jpg", image, [int(cv2.IMWRITE_JPEG_QUALITY), self.jpeg_quality])

        self.sender.send_jpg(msg, jpg_buffer)

    def publish_str(self, msg):
        self.sender.zmq_socket.send_string(msg)

    def close(self):
        self.sender.close()  # close the ZMQ socket and context


if __name__ == '__main__':
    publisher = Publisher()

    rpi_name = socket.gethostname()  # send RPi hostname with each image

    subprocess.call("v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_auto_priority=1 -c exposure_absolute=500", shell=True)
    #subprocess.call("v4l2-ctl -d /dev/video0 -c exposure_auto=1", shell=True)

    vc = cv2.VideoCapture(0)
    vc.set(cv2.CAP_PROP_FRAME_WIDTH, 848) # more FOV than with 640
    vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    vc.set(cv2.CAP_PROP_FPS, 30)


    time.sleep(1.0)  # allow camera sensor to warm up

    count = 0

    #car = ev3.TwoWheelVehicle(
    #    0.028,  # radius_wheel
    #    0.161,  # tread
    #    protocol=ev3.USB
    #)
    cps = CountsPerSec().start()

    try:
        while True:  # send images as stream until Ctrl-C
            rval, image = vc.read()
            #image = Image.fromarray(image)
            xSoll = -0.308
            x = posEst(image)
#            print("x: ",x)
#            if x is not None:
#                if x < xSoll:
#                    parcours = (
#    #                    car.drive_turn(-2, 0.0, speed=90) 
#                    )
#                    parcours.start(thread=True)
#                    parcours.join()
#                else:
#                    parcours = (
#    #                    car.drive_turn(2, 0.0, speed=90) 
#                    )
#                    parcours.start(thread=True)
#                    parcours.join()
#
#            sleep(.1)

            image = imagePro(image)
            msg = Message()
            msg.id = count
            # msg = rpi_name + " " + str(count)
            msg_str = jsonpickle.encode(msg)

            image = putIterationsPerSec(image, cps.countsPerSec())

            publisher.publish_img(msg_str, image)
            cps.increment()
            count += 1
    except (KeyboardInterrupt, SystemExit):
        pass  # Ctrl-C was pressed to end program
    except Exception as ex:
        print('Python error with no Exception handler:')
        print('Traceback error:', ex)
        traceback.print_exc()
    finally:
        vc.release()  # stop the camera thread
        publisher.close()
        sys.exit()
