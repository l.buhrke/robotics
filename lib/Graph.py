import cv2

def draw(img, corners, imgpts):
    corner = tuple(corners.ravel())
    #print(corner)
    #corner = tuple((corners[0].ravel() + corners[1].ravel()) * 0.5)
    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255,0,0), 5)
    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0,255,0), 5)
    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0,0,255), 5)
    return img

def poly(img, corner):
    img = cv2.line(img, corner[0], corner[1], (255, 0, 0), 5)
    img = cv2.line(img, corner[1], corner[2], (255, 0, 0), 5)
    img = cv2.line(img, corner[2], corner[3], (255, 0, 0), 5)
    img = cv2.line(img, corner[3], corner[0], (255, 0, 0), 5)
    return img



