import socket
import cv2
import subprocess
import time
from multiprocessing import Process
from time import sleep

class VideoGet:
    """
    Class that continuously gets frames from a VideoCapture object
    with a dedicated thread.
    """

    def __init__(self, src=0):


        #subprocess.call("v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_auto_priority=1 -c exposure_absolute=1000", shell=True)
        subprocess.call("v4l2-ctl -d /dev/video0 -c exposure_auto=1", shell=True)

        self.stream = cv2.VideoCapture(src)
        self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 848) # more FOV than with 640
        self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        self.stream.set(cv2.CAP_PROP_FPS, 30)


        time.sleep(1.0)  # allow camera sensor to warm up


        (self.grabbed, self.frame) = self.stream.read()
        self.stopped = False

    def start(self):    
        p = Process(target=self.get, args=())
        p.start()
        p.join()
        return self

    def get(self):
        while not self.stopped:
            if not self.grabbed:
                self.stop()
            else:
                (self.grabbed, self.frame) = self.stream.read()

    def stop(self):
        self.stopped = True

