import subprocess
import cv2
import numpy as np
import lib.Graph as graph
from lib.utiles import ARUCO_DICT

mtx = np.array([635.93, 0.0, 444.27,
                0.0, 634.263, 256.835,
                0.0, 0.0, 1.0])
mtx = np.reshape(mtx, (3, 3))

# mtx = np.load("lib/CameraMatrix_Rob.npy")
dis = np.load("lib/disortionVektor_Rob.npy")
parameters = cv2.aruco.DetectorParameters_create()

aruco_dict_type = ARUCO_DICT["DICT_ARUCO_ORIGINAL"]
cv2.aruco_dict = cv2.aruco.Dictionary_get(aruco_dict_type)


M = cv2.getRotationMatrix2D((848 // 2, 480 // 2), -180, 1.0)
axis = np.float32([[0.05 / 2, 0, 0], [0, 0.05 / 2, 0], [0, 0, 0.05 / 2]]).reshape(-1, 3)


def imagePro(frame):
    shape = np.shape(frame)
    frame = cv2.warpAffine(frame, M, (848, 480))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # print(np.shape(frame), np.shape(gray))

    corners, ids, rejected_img_points = cv2.aruco.detectMarkers(gray, cv2.aruco_dict, parameters=parameters)

    for i in range(len(corners)):
        rvec, tvec, _ = cv2.aruco.estimatePoseSingleMarkers(corners[i], 0.05, mtx, dis)
        #        print("rv: ", rvec)
        #        print("tc: ", tvec)
        imgpts, jac = cv2.projectPoints(axis, rvec, tvec, mtx, dis)
        imgpts = imgpts.astype(int)

        corn = np.array([(corners[i][0][0][0] + corners[i][0][1][0] + corners[i][0][2][0] + corners[i][0][3][0]) / 4, (corners[i][0][0][1] + corners[i][0][1][1] + corners[i][0][2][1] + corners[i][0][3][1]) / 4], dtype=int)
        frame = graph.poly(frame, corners[i][0].astype(int))
        frame = graph.draw(frame, corn, imgpts)
        s = str(ids[i][0])
        cv2.putText(frame, s, corn, cv2.FONT_HERSHEY_PLAIN, 3, (0, 0, 255), thickness=3)

    frame = cv2.resize(frame, None, fx=1.92, fy=1.92, interpolation=cv2.INTER_AREA)
    return frame