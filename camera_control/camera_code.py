import subprocess
import time
import cv2
import numpy as np
from lib.utiles import ARUCO_DICT
from camera_control.DiscDetection import detectDisc


mtx = np.array([635.93, 0.0, 444.27,
                0.0, 634.263, 256.835,
                0.0, 0.0, 1.0])
mtx = np.reshape(mtx, (3, 3))

# mtx = np.load("lib/CameraMatrix_Rob.npy")
dis = np.load("lib/disortionVektor_Rob.npy")
parameters = cv2.aruco.DetectorParameters_create()

aruco_dict_type = ARUCO_DICT["DICT_ARUCO_ORIGINAL"]
cv2.aruco_dict = cv2.aruco.Dictionary_get(aruco_dict_type)


def posEst(frame):
    M = cv2.getRotationMatrix2D((848 // 2, 480 // 2), -180, 1.0)
    frame = cv2.warpAffine(frame, M, (848, 480))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # print(np.shape(frame), np.shape(gray))
    # print(np.shape(frame), np.shape(gray))

    corners, ids, rejected_img_points = cv2.aruco.detectMarkers(gray, cv2.aruco_dict, parameters=parameters)

    rvec, tvec, markerPoints = cv2.aruco.estimatePoseSingleMarkers(corners, 0.05, mtx, dis)

    if len(corners) > 0:
        return ids, tvec

    return [], []


def camera_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    # #  INIT CAM ##

    #subprocess.call('v4l2-ctl -d /dev/video0 -c exposure_auto=0 exposure_auto_priority=1 exposure_absolute=300', shell=True)
    subprocess.call('v4l2-ctl -d /dev/video0 -c exposure_auto=1 -c exposure_auto_priority=0 -c exposure_absolute=50', shell=True)

    vc = cv2.VideoCapture(0)
    vc.set(cv2.CAP_PROP_FRAME_WIDTH, 848)  # more FOV than with 640
    vc.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    vc.set(cv2.CAP_PROP_FPS, 30)

    time.sleep(1.0)  # allow camera sensor to warm up
    while True:
        (rval, image) = vc.read()
        ids, tvec = posEst(image)
        greendisc_list, reddisc_list = detectDisc(image)
        if remote_a.poll(0):
            name_r, (cmd, data) = remote_a.recv()
            if cmd == 'get_camera_data':
                remote_a.send((name_r, (ids, tvec)))
            elif cmd == 'get_image':
                remote_a.send((name_r, image))
            elif cmd == 'close':
                remote_a.close()
                break