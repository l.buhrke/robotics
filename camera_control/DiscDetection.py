import numpy as np
import cv2 as cv

disc3D = np.array([[0, 0.025, 0], [0.025, 0, 0], [0.0, -0.025, 0], [-0.025, 0.0, 0]])
M = cv.getRotationMatrix2D((848 // 2, 480 // 2), -180, 1.0)

mtx = [635.93, 0.0, 444.27,
       0.0, 634.263, 256.835,
       0.0, 0.0, 1.0]

mtx = np.reshape(mtx, (3, 3))
dist = np.array([-0.372403, 0.157163, -0.00424357, -0.00231644, -0.0522724])

lower_green = np.array([36, 60, 90])
upper_green = np.array([48, 255, 255])

lower_red1 = np.array([172, 85, 50])
upper_red1 = np.array([181, 255, 255])
lower_red2 = np.array([0, 85, 50])
upper_red2 = np.array([3, 255, 255])
kernel = np.ones((3, 3), np.uint8)

#### CREATE BLOB DETECTOR
params = cv.SimpleBlobDetector_Params()

params.filterByColor = True
params.blobColor = 255

params.filterByArea = True
params.minArea = 250
params.maxArea = 480 * 848

params.filterByCircularity = True
params.minCircularity = 0.08

params.filterByConvexity = False
params.minConvexity = 0.05

params.filterByInertia = True
params.minInertiaRatio = 0.05

detector = cv.SimpleBlobDetector_create(params)

## storage for arrays
hsv = np.zeros((480, 848, 3), np.uint8)
mask_g = np.zeros((480, 848), np.uint8)
mask_r1 = np.zeros((480, 848), np.uint8)
mask_r2 = np.zeros((480, 848), np.uint8)
finalMask = np.zeros((480, 848), dtype=np.uint8)


def detectDisc(img):
    ############# manipulate input img #############
    img = cv.warpAffine(img, M, (848, 480))  # !!!!!!!!!!!!Only if not rotated yet!!!!!!!!!!
    img[:, :, :] = cv.bilateralFilter(img, 12, 20, 15)
    hsv[:, :, :] = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    ############# MAKE MASKS ############
    mask_g[:, :] = cv.inRange(hsv, lower_green, upper_green)
    mask_r1[:, :] = cv.inRange(hsv, lower_red1, upper_red1)
    mask_r2[:, :] = cv.inRange(hsv, lower_red2, upper_red2)
    mask_r1[:, :] = mask_r1 + mask_r2
    mask_r1[mask_r1 > 255] = 255
    mask_g[:, :] = cv.GaussianBlur(mask_g, (0, 0), sigmaX=2.5, sigmaY=2)
    mask_r1[:, :] = cv.GaussianBlur(mask_r1, (0, 0), sigmaX=2.5, sigmaY=2)
    mask_g[mask_g > 250] = 255
    mask_r1[mask_r1 > 250] = 255
    mask_g[mask_g <= 250] = 0
    mask_r1[mask_r1 <= 250] = 0
    mask_g[:, :] = cv.dilate(mask_g, kernel, iterations=5)
    mask_r1[:, :] = cv.dilate(mask_r1, kernel, iterations=5)

    ############ Detect blobs #############
    keypoints_g = detector.detect(mask_g)
    keypoints_r = detector.detect(mask_r1)

    greendisc_list = []

    for keypoint in keypoints_g:
        # print(keypoint.pt)
        # print(keypoint.size)
        finalMask[:, :] = 0
        middle = np.array([int(keypoint.pt[0]), int(keypoint.pt[1])])

        finalMask[:, :] = cv.circle(finalMask, middle, int(keypoint.size * 2.0), 255, -1)
        finalMask[:, :] = (finalMask // 255) * mask_g
        indexes = np.argwhere(finalMask >= 255)
        if (len(indexes) != 0):
            y_max = np.max(indexes[:, 0])
            x_max = np.max(indexes[:, 1])
            y_min = np.min(indexes[:, 0])
            x_min = np.min(indexes[:, 1])

            down = np.array([(x_max + x_min) // 2, y_max])
            up = np.array([(x_max + x_min) // 2, y_min])
            right = np.array([x_max, (y_max + y_min) // 2])
            left = np.array([x_min, (y_max + y_min) // 2])

            points = np.array([down, right, up, left], dtype=float)

            rvec, tvec, error = cv.solvePnP(disc3D, points, mtx, dist)
            greendisc_list.append(tvec)

    reddisc_list = []
    for keypoint in keypoints_r:
        # print(keypoint.pt)
        # print(keypoint.size)
        finalMask[:, :] = 0
        middle = np.array([int(keypoint.pt[0]), int(keypoint.pt[1])])

        finalMask[:, :] = cv.circle(finalMask, middle, int(keypoint.size * 2.0), 255, -1)
        finalMask[:, :] = (finalMask // 255) * mask_r1
        indexes = np.argwhere(finalMask >= 255)
        if (len(indexes) != 0):
            y_max = np.max(indexes[:, 0])
            x_max = np.max(indexes[:, 1])
            y_min = np.min(indexes[:, 0])
            x_min = np.min(indexes[:, 1])

            down = np.array([(x_max + x_min) // 2, y_max])
            up = np.array([(x_max + x_min) // 2, y_min])
            right = np.array([x_max, (y_max + y_min) // 2])
            left = np.array([x_min, (y_max + y_min) // 2])

            points = np.array([down, right, up, left], dtype=float)

            rvec, tvec, error = cv.solvePnP(disc3D, points, mtx, dist)
            reddisc_list.append(tvec)

    return greendisc_list, reddisc_list
