import cv2
import numpy as np
from camera_control.camera_code import posEst


def camera_control_dummy(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    matrix_zeros = np.zeros((848, 480), dtype=np.uint8)
    matrix_zeros = np.expand_dims(matrix_zeros, -1)
    image = cv2.cvtColor(matrix_zeros, cv2.COLOR_GRAY2BGR)
    while True:
        ids, tvec = posEst(image)
        if remote_a.poll(0):
            name_r, (cmd, data) = remote_a.recv()
            if cmd == 'get_camera_data':
                remote_a.send((name_r, (ids, tvec)))
            elif cmd == 'get_image':
                remote_a.send((name_r, image))
            elif cmd == 'close':
                remote_a.close()
                break