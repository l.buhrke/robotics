import itertools
import math
import time
from collections import defaultdict
from copy import copy

import cv2 as cv
import numpy as np
from typing import List, Set

from planning.utils import angle_between, l2, lineseg_dists


class CNode:
    def __init__(self, pos, ident, err):
        self.pos: np.array([int, int]) = np.array(pos)
        self.possible_nb: List[CNode] = []
        self.id = ident
        self.critical = False
        self.angle = 0
        self.target = False
        self.error = err

    def distance(self, other):
        return math.sqrt(math.pow(self.pos[0] - other.pos[0], 2) + math.pow(self.pos[1] - other.pos[1], 2))

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return f'(node: {self.id})'

    def __repr__(self):
        return f'(node: {self.id})'


class CGraph(object):
    """ Graph data structure, undirected by default. """

    def __init__(self, connections=None, directed=False):
        self.graph: defaultdict[CNode: Set[CNode]] = defaultdict(set)
        self._directed = False
        self.new_connections = {}
        self.min_node_distance = 0
        self.max_node_distance = 30
        self.critical_angle = np.pi / 2
        self.impossible_angle = 45 * (2*np.pi) / 360
        self.d_w = 3
        self.a_w = 1
        if connections is not None:
            self.add_connections(connections)

    def add_connections(self, connections):
        """ Add connections (list of tuple pairs) to graph """

        for node1, node2 in connections:
            self.add(node1, node2)

    def remove(self, node1, node2):
        try:
            if node1 in self.graph:
                self.graph[node1].remove(node2)
        except KeyError:
            pass
        try:
            if node2 in self.graph:
                self.graph[node2].remove(node1)
        except KeyError:
            pass

    def fill_possible_nb_list(self, already_connected=2):
        for i, (original_node, value) in enumerate(self.graph.items()):
            original_node.possible_nb = []
            for connected in self.graph[original_node]:
                original_node.possible_nb.append(connected)
            for i, (node, value) in enumerate(self.graph.items()):
                if original_node == node:
                    continue
                if self.min_node_distance < original_node.distance(node) < self.max_node_distance:
                    if node not in original_node.possible_nb and len(self.graph[node]) < already_connected:
                        original_node.possible_nb.append(node)

    def create_node_system(self):
        # search for nb
        self.remove_all_connections()
        for j in range(1):
            self.fill_possible_nb_list()
            self.sort_cnx_preferences()
            for i, (node_m, value) in enumerate(self.new_connections.items()):
                if len(value) == 0:
                    continue
                [(node1, node2, node), _] = value[0]
                self.add(node, node1)
                self.add(node, node2)

            self.delete_third_node()

        # fits lonely points into close cnx
        for _ in range(1):
            self.fill_possible_nb_list(100)
            for i, (node_m, value) in enumerate(self.graph.items()):
                if len(value) == 0 and len(node_m.possible_nb) > 0:
                    best_fit = []
                    for nb in node_m.possible_nb:
                        for nb_nb in self.graph[nb]:
                            d = lineseg_dists(node_m.pos, np.expand_dims(nb.pos, 0), np.expand_dims(nb_nb.pos, 0))
                            best_fit.append([(nb, nb_nb), np.abs(d)])

                    best_fit.sort(key=lambda e: e[1])
                    if len(best_fit) == 0:
                        continue
                    node_1, node2 = best_fit[0][0]
                    self.remove(node_1, node2)
                    angle = angle_between(np.array(node_1.pos - node_m.pos), np.array(node2.pos - node_m.pos))
                    node_m.angle = angle
                    self.add(node_m, node_1)
                    self.add(node_m, node2)

        # connect lose ends / points
        for _ in range(1):
            self.fill_possible_nb_list(100)
            for i, (node_m, cnx_list) in enumerate(self.graph.items()):
                if len(cnx_list) >= 1:  # you are done
                    continue
                possible_nb_list = []
                for nb in node_m.possible_nb:  # check if you have a nb
                    if len(self.graph[nb]) >= 1:  # he is done:
                        continue
                    # possible nb found
                    possible_nb_list.append([nb, node_m.distance(nb)])

                possible_nb_list.sort(key=lambda e: e[1], reverse=False)
                if len(possible_nb_list) == 0:
                    continue
                node_best = possible_nb_list[0][0]
                self.add(node_m, node_best)

        self.update_angle()

    def update_angle(self):
        self.new_connections = {}
        for i, (node, value) in enumerate(self.graph.items()):
            node.critical = False
            if len(self.graph[node]) < 2:
                node.critical = True
                continue

            node1, node2 = list(self.graph[node])[0], list(self.graph[node])[1]
            angle = angle_between(np.array(node1.pos - node.pos), np.array(node2.pos - node.pos))
            node.angle = angle
            if angle < self.critical_angle:
                node.critical = True

    def sort_cnx_preferences(self):
        self.new_connections = {}
        for i, (node, value) in enumerate(self.graph.items()):
            if len(value) == 2:
                continue
            combinations = list(itertools.combinations(node.possible_nb, r=2))
            best_combination = []
            for node1, node2 in combinations:
                if (len(self.graph[node1]) == 2 and node1 not in self.graph[node])\
                        or (len(self.graph[node2]) == 2 and node1 not in self.graph[node2]):
                    continue
                angle = angle_between(np.array(node1.pos - node.pos), np.array(node2.pos - node.pos))
                if angle < self.impossible_angle:
                    continue
                distance = node.distance(node1) + node.distance(node2)
                best_combination.append([(node1, node2, node), self.a_w * (angle / math.pi) - self.d_w * (distance / self.max_node_distance)])
            self.new_connections[node] = sorted(best_combination, key=lambda x: x[1], reverse=True)

    def remove_all_connections(self):
        for node in self.graph:
            self.graph[node]: Set[CNode] = set()

    def add_node(self, node):
        self.graph[node]: Set[CNode] = set()

    def delete_one_connections(self):
        for node in self.graph.keys():
            if len(self.graph[node]) == 1:
                self.remove(node, list(self.graph[node])[0])

    def delete_third_node(self, i=0):
        cgraph = copy(self.graph)
        for node in cgraph.keys():
            if len(self.graph[node]) > 2:
                combinations = list(itertools.combinations(self.graph[node], r=2))
                highest_v = -999
                best_combination = None
                for node1, node2 in combinations:
                    if node1 == node2:
                        continue
                    angle = angle_between(np.array(node1.pos - node.pos), np.array(node2.pos - node.pos))
                    distance = node.distance(node1) + node.distance(node2)
                    v = self.a_w * (angle / math.pi) - self.d_w * (distance / self.max_node_distance)
                    if v > highest_v:
                        highest_v = v
                        best_combination = (node1, node2, node)
                if best_combination is not None:
                    self.remove_node(node)
                    self.add_node(node)
                    self.add(node, best_combination[0])
                    self.add(node, best_combination[1])
        return i

    def add(self, node1, node2):
        self.graph[node1].add(node2)
        if not self._directed:
            self.graph[node2].add(node1)

        return True

    def remove_node(self, node):
        """ Remove all references to node """

        for n, cxns in self.graph.items():  # python3: items(); python2: iteritems()
            try:
                cxns.remove(node)
            except KeyError:
                pass
        try:
            del self.graph[node]
        except KeyError:
            pass

    def is_in_graph(self, node):
        return node in self.graph

    def is_connected(self, node1, node2):
        """ Is node1 directly connected to node2 """

        return node1 in self.graph and node2 in self.graph[node1]

    def __str__(self):
        return '{}({})'.format(self.__class__.__name__, dict(self.graph))

    def get_critical_nodes(self, pos):
        critical_node = []
        for key in self.graph:
            if key.critical:
                d = l2(key.pos, pos)
                critical_node.append([key, d])
        return critical_node

    def get_nodes_around_player(self, pos, eps):
        nodes_list = []
        for key in self.graph:
            if l2(key.pos, pos) < eps:
                nodes_list.append(key)

        return nodes_list

    def get_list_of_cnx(self):
        starting_points = []
        list_of_cnx = []
        for (key, cnxs) in self.graph.items():
            if len(cnxs) < 2:
                starting_points.append(key)
        if len(starting_points) == 0:  # all connected
            pass
        for key in starting_points:
            list_of_cnx.append(key)
            prev_node = key
            while True:
                cnx = self.graph[prev_node]
                if len(cnx) == 1 and key is not prev_node:
                    break
                for pos_node in cnx:
                    if pos_node is not prev_node:
                        list_of_cnx.append(pos_node)
                        prev_node = pos_node
                        break

    def draw(self, map, with_text=True, scale=5):
        for key in self.graph:  # key is node
            for nb in self.graph[key]:  # nb is node
                map = cv.line(map, key.pos, nb.pos, color=[255, 0, 255], thickness=scale * 2)
                if len(self.graph[key]) == 1:
                    map = cv.line(map, key.pos, key.pos + 0 * (key.pos - nb.pos), color=[255, 0, 255], thickness=scale * 2)
        if with_text:
            for key in self.graph:
                if key.critical:

                    map = cv.circle(img=map, center=key.pos, color=[0, 0, 255], thickness=-1, radius=2)
                    map = cv.putText(img=map, text=f'{key.id}', org=key.pos + np.array((0, -10)), color=[0, 0, 255],
                                     fontScale=.4, fontFace=cv.FONT_ITALIC)
                else:
                    map = cv.circle(img=map, center=key.pos, color=[0, 255, 0], thickness=-1, radius=2)
                    map = cv.putText(img=map, text=f'{key.id}', org=key.pos + np.array((0, -10)), color=[0, 255, 0],
                                     fontScale=.4, fontFace=cv.FONT_ITALIC)
        return map

    def get_connections(self):
        connections = []
        for (key, cnxs) in self.graph.items():
            for cnx in cnxs:
                connections.append([key.pos, cnx.pos])
        return connections

    def mean_position(self):
        sum_array = np.array([0, 0])
        index = 0
        for index, (key, _) in enumerate(self.graph.items()):
            sum_array += key.pos
        if index == 0:
            return None
        return sum_array/(index + 1)


if __name__ == '__main__':
    pass
