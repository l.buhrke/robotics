from planning.planning import CPlan


def planning_dummy(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    planner = CPlan()
    # print("planning started")
    path = []
    cnx_t = ([], [], [])

    while True:
        if remote_a.poll(0):
            name_r, (cmd, data) = remote_a.recv()
            if cmd == "target_reached":
                target_reached = True
                print('target_reached')
            elif cmd == "get_image":
                remote_a.send((name_r, planner.draw()))
            elif cmd == 'get_path_and_connections':
                remote_a.send((name_r, (path, cnx_t)))
            else:
                print(cmd, data)
                raise NotImplementedError