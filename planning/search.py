from collections import deque
import numpy as np
from planning.utils import l2, PriorityQueue


def bfs(start_position: np.array, obstacle_c, map, scale):
    possible_moves = [np.array([0, 1]), np.array([0, -1]), np.array([1, 0]), np.array([-1, 0]),
                      np.array([-1, -1]), np.array([-1, 1]), np.array([1, 1]), np.array([1, -1])]
    explored_set = set()
    open_set = deque()

    reconstructed_path = {start_position.tobytes(): []}
    open_set.append(start_position)
    while open_set:
        node = open_set.popleft()
        if not np.all(map[node[1], node[0]] == obstacle_c):
            return reconstructed_path[node.tobytes()]

        explored_set.add(node.tobytes())
        track = reconstructed_path[node.tobytes()]
        for action in possible_moves:
            new_position = node + action * scale
            if new_position.tobytes() in explored_set:
                continue
            reconstructed_path[new_position.tobytes()] = track + [new_position]
            open_set.append(new_position)
    return []


class elements:
    def __init__(self, state: np.array):
        self.state: np.array = state
        self.predecessor = None

    def __hash__(self):
        return hash(self.state.tobytes())

    def __eq__(self, other):
        return self.__hash__() == hash(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f'{self.state}'


def reconstruct_path(node):
    track = [node.state]
    next_node = node.predecessor
    while next_node is not None:
        track.append(next_node.state)
        next_node = next_node.predecessor
    track.reverse()
    return track



# TODO first to reach a disc node moves it correctly on their position
def a_star(start_position, goal_position, map, scale=10, obstacle=None, patience=5, Fine=False, shift=0):
    if obstacle is None:
        obstacle = [255, 255, 255]

    frontier = PriorityQueue([(l2(start_position, goal_position), elements(start_position))])
    explored_set = set()
    open_set = set()
    open_set.add(elements(start_position))
    track_costs = {elements(start_position): 0}

    possible_moves = [np.array([0, 1]), np.array([0, -1]), np.array([1, 0]), np.array([-1, 0]),
                      np.array([-1, -1]), np.array([-1, 1]), np.array([1, 1]), np.array([1, -1])]
    norm_list = [np.linalg.norm(action * scale) for action in possible_moves]

    while frontier:
        _, node = frontier.pop()
        open_set.remove(node)
        if l2(goal_position, node.state) <= (scale * 4 + shift):
            return reconstruct_path(node), _, track_costs[node]  # path, map, cost

        explored_set.add(node)
        costs = track_costs[node]
        for i, action in enumerate(possible_moves):
            new_position = node.state + action * scale
            new_element = elements(new_position)

            if new_element in explored_set:
                continue

            if np.all(map[new_position[1], new_position[0]] == obstacle):
                continue

            if new_element in track_costs and (costs + norm_list[i]) > track_costs[new_element]:
                continue

            new_element.predecessor = node
            track_costs[new_position.tobytes()] = costs + norm_list[i]
            if new_element in open_set:
                frontier.update_elem(new_element, (costs + norm_list[i] + l2(new_position, goal_position), new_element))
            else:
                frontier.put((costs + norm_list[i] + l2(new_position, goal_position), new_element))
                open_set.add(new_element)

    return None, None, None
