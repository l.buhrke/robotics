import math
import time
from collections import deque
import numpy as np
from planning.cgraph import CGraph, CNode
from planning.utils import l2
from planning.search import a_star, bfs
import cv2 as cv
from skimage.draw import line


class CPlayer:
    def __init__(self, pos):
        self.pos = np.array(pos)
        self.radius = 10
        self.id = -10
        self.antenna = 30 / 360 * (2 * math.pi)
        self.alpha = 0
        self.antenna_l = 70
        self.ap1 = [0, 0]
        self.ap2 = [0, 0]

    def update(self, pos, alpha):
        self.pos = pos
        self.alpha = abs(alpha - 2 * math.pi)
        self.ap2 = [int(self.pos[0] + self.antenna_l * math.cos(self.alpha + self.antenna)), int(self.pos[1] - self.antenna_l * math.sin(self.alpha + self.antenna))]
        self.ap1 = [int(self.pos[0] + self.antenna_l * math.cos(self.alpha - self.antenna)), int(self.pos[1] - self.antenna_l * math.sin(self.alpha - self.antenna))]

    def react(self, map):
        p1 = self.pos

        rr, cc = line(*p1, *self.ap2)
        l, r = 0, 0
        maxl = len(rr) - 1
        for l in range(len(rr)):
            if np.all(map[cc[l], rr[l]] == [255, 0, 255]):  # line intersects with obstacle
                break

        rr, cc = line(*p1, *self.ap1)
        maxr = len(rr) - 1
        for r in range(len(rr)):
            if np.all(map[cc[r], rr[r]] == [255, 0, 255]):  # line intersects with obstacle
                break

        return [1 - r / maxr, 1 - l / maxl]

    def draw(self, map):
        map = cv.circle(img=map, center=self.pos, radius=self.radius, thickness=-1, color=[100, 0, 0])
        map = cv.line(img=map, pt1=self.pos, pt2=np.array(self.ap2), color=[0, 100, 0], thickness=3)
        map = cv.line(img=map, pt1=self.pos, pt2=np.array(self.ap1), color=[0, 100, 0], thickness=3)
        return map

    def __cmp__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id


class CPlan:
    def __init__(self):
        self.border_size = (900, 900, 3)
        self.border_color = [255, 0, 255]
        self.min_dx = 15
        self.min_y = 900 - self.min_dx
        self.min_x = 900 - self.min_dx
        self.max_y = 0
        self.max_x = 0

        self.track_file = None
        self.map = self.init_map()

        self.start_position = 450
        # print(np.unique(img.reshape(-1, img.shape[2]), axis=0, return_counts=True))
        self.goal_node = CNode(pos=(70, 70), ident=9000, err=0)  # np.argwhere(np.all(self.map == [0, 255, 0], axis=-1))[0]
        self.start_node = CNode(pos=(70, 70), ident=9000, err=0)


        self.lgraph = CGraph()
        self.rgraph = CGraph()
        self.ograph = CGraph()
        self.sgraph = CGraph()
        self.fgraph = CGraph()

        self.player = CPlayer((self.start_position, self.start_position))
        self.explored_nodes = set()
        self.constructed_path = None
        self.current_target = self.player
        self.last_target = self.player
        self.start_node = None
        self.old_cost = 1000

        self.fine_search_size = 5
        self.coarse_search_size = 5
        self.minimum_distance = 12
        self.scale = self.coarse_search_size

        self.error_search_radius = 40
        self.error_trigger = 0.30
        self.error_flag = False
        self.maximum_error = 0.15

        self.exploration = True
        self.racing = False
        self.reaction = None
        self.backwards = False

    def set_goal_and_start(self):
        start_pos_a = self.sgraph.mean_position()
        goal_pos = self.fgraph.mean_position()
        start_pos = start_pos_a + 1 * (start_pos_a - goal_pos)
        self.goal_node = CNode(pos=goal_pos, ident=-10, err=0)
        self.start_node = CNode(pos=start_pos, ident=-11, err=0)

    def init_map(self):
        if self.track_file is None:
            map = np.ones(self.border_size) * 255
            map[:, :] = self.border_color
            dx = max(self.max_x - self.min_x, 0)
            dy = max(self.max_y - self.min_y, 0)
            map[(self.min_y - self.min_dx):(self.min_y + dy + self.min_dx),
                (self.min_x - self.min_dx):(self.min_x + dx + self.min_dx)] = np.ones((dy + 2*self.min_dx, dx + 2*self.min_dx, 3)) * 255
            return map
        img = cv.imread(self.track_file)
        return img

    def correct_path(self, paths, robot_position):  # maybe just correct while racing
        new_paths = []
        for path in paths:
            self.draw_without_text(max(self.scale, self.minimum_distance))
            p1 = np.array([int(self.start_position + robot_position[0] * 100), int(self.start_position + robot_position[1] * 100)])
            # p1 = self.player.pos
            new_path = []
            corrections = 0
            max_corrections = 30
            for i, p2 in enumerate(path):
                '''if i == 0:
                    continue
                new_path.append(path[i - 1])'''
                rr, cc = line(*p1, *p2)
                if np.any(np.all(self.map[cc, rr] == np.array([[255, 0, 255]]), axis=1)):  # line intersects with obstacle
                    if i == 0:
                        continue
                    new_path.append(path[i - 1])
                    p1 = path[i - 1]
                    corrections += 1
                if corrections == max_corrections:
                    break
            if len(path) > 1:
                new_path.append(path[-1])

            new_paths.append(list((np.array(new_path) - self.start_position) / 100))
        return new_paths

    def update_min_max_xy(self, pos):
        if not (self.min_x < pos[0] < self.max_x):
            if self.min_x > pos[0]:
                self.min_x = pos[0] - 15
            if self.max_x < pos[0]:
                self.max_x = pos[0] + 15

        if not (self.min_y < pos[1] < self.max_y):
            if self.min_y > pos[1]:
                self.min_y = pos[1] - 15
            if self.max_y < pos[1]:
                self.max_y = pos[1] + 15

    def update_nodes_and_player(self, player_position, theta, landmarks):
        self.lgraph = CGraph()
        self.rgraph = CGraph()
        self.ograph = CGraph()
        self.sgraph = CGraph()
        self.fgraph = CGraph()
        for node_id, tpos in landmarks.items():
            pos, err = tpos
            self.add_node(node_id, (int(self.start_position + pos[0] * 100), int(self.start_position + pos[1] * 100)), err)
        self.lgraph.create_node_system()
        self.rgraph.create_node_system()
        self.ograph.create_node_system()
        self.sgraph.create_node_system()
        self.fgraph.create_node_system()
        self.player.update(np.array([int(self.start_position + player_position[0] * 100), int(self.start_position + player_position[1] * 100)]),
                           theta)

        self.update_min_max_xy(self.player.pos)

    def handle_player_error(self, p_err):
        if (p_err[0] * p_err[1]) > self.error_trigger:
            self.error_flag = True
            self.old_cost = float('inf')

        if self.error_flag:
            self.set_error_reduction_target()
            if (p_err[0] * p_err[1]) < self.maximum_error or self.old_cost < (self.fine_search_size + self.minimum_distance):
                self.error_flag = False

    def add_node(self, node_id, pos, err):
        self.update_min_max_xy(pos)
        node = CNode(ident=node_id, pos=pos, err=err[0] * err[1])

        if node == self.current_target:
            self.current_target = node  # update position

        if self.start_node is None:
            self.start_node = node

        # TODO start and finish line
        if 0 < node_id < 33:  # start line
            self.sgraph.add_node(node)
            return
        elif 33 < node_id < 66:  # finish line
            self.fgraph.add_node(node)
            return

        if node_id % 3 == 0:  # left wall grün
            self.lgraph.add_node(node)
        elif node_id % 3 == 1:  # obstacle  # rot
            self.ograph.add_node(node)
        else:  # right wall  # blau
            self.rgraph.add_node(node)

    def step_test(self, landmarks: dict[int, tuple[tuple[float, float], tuple[float, float, float]]],
             player_position_data, theta: float):  # theta, 0 2pi
        player_position, p_err = player_position_data
        self.reaction = None
        self.update_nodes_and_player(player_position, theta, landmarks)

    def step(self, landmarks: dict[int, tuple[tuple[float, float], tuple[float, float, float]]],
             player_position_data, theta: float):  # theta, 0 2pi

        player_position, p_err = player_position_data
        self.reaction = None
        self.update_nodes_and_player(player_position, theta, landmarks)
        self.handle_player_error(p_err)

        print(self.current_target, self.old_cost)
        if self.current_target is not None and self.old_cost < (self.fine_search_size*5 + self.minimum_distance):
            self.explored_nodes.add(self.current_target)
            self.last_target = self.current_target
            self.current_target = None

        path = self.get_next_path()
        return path

    def get_next_path(self):
        if self.racing:
            self.current_target = self.goal_node
            self.player.pos = self.start_node.pos
            path, _ = self.call_a_star(self.current_target)
            return path
        return self.set_exploration_target()

    def call_a_star(self, current_target):
        # depending on distance -> choose search step size
        self.scale = self.fine_search_size if l2(self.player.pos, current_target.pos) < (5 * self.coarse_search_size + max(0, self.minimum_distance - self.coarse_search_size)) \
            else self.coarse_search_size

        # prepare map on which the algorithm runs on
        self.draw_without_text(max(self.minimum_distance, self.scale))
        # if player is in obstacle line move out / set player.pos to the closest drivable place
        # thus the roboter automatically moves out of the zone
        self.constructed_path = bfs(self.player.pos, obstacle_c=[255, 0, 255], map=self.map,
                                    scale=self.fine_search_size)
        if len(self.constructed_path) > 0:
            self.player.pos = self.constructed_path[-1]

        result = a_star(self.player.pos, current_target.pos, self.map, scale=self.scale, obstacle=[255, 0, 255],
                        patience=1_000_000_000, Fine=self.scale == self.fine_search_size, shift=max(0, self.minimum_distance - self.scale))
        constructed_path, _, costs = result
        #if constructed_path is not None:
            #constructed_path.append(current_target.pos)
        self.reaction = self.player.react(self.map)
        return constructed_path, costs

    def draw_without_text(self, scale):
        self.map = self.init_map()
        self.map = self.lgraph.draw(self.map, with_text=False, scale=scale)
        self.map = self.rgraph.draw(self.map, with_text=False, scale=scale)
        self.map = self.ograph.draw(self.map, with_text=False, scale=scale)
        self.map = self.sgraph.draw(self.map, with_text=False, scale=scale)
        self.map = self.fgraph.draw(self.map, with_text=False, scale=scale)

    def set_error_reduction_target(self):
        self.current_target = self.start_node

    def print_critical_nodes(self):
        l_critical = self.lgraph.get_critical_nodes(self.player.pos)
        r_critical = self.rgraph.get_critical_nodes(self.player.pos)
        to_explore = l_critical + r_critical
        print(to_explore)

    def set_exploration_target(self):
        l_critical = self.lgraph.get_critical_nodes(self.player.pos)
        r_critical = self.rgraph.get_critical_nodes(self.player.pos)
        to_explore = l_critical + r_critical

        to_explore.sort(key=lambda x: x[1], reverse=False)
        # print(to_explore)
        if len(to_explore) == 0:
            if not (self.fgraph.mean_position() is None or self.sgraph.mean_position() is None):
                self.racing = True
                self.exploration = False

            paths = [[[], float('inf'), self.player]]
        else:
            paths = []
        for [node, d] in to_explore:
            if node is None or node in self.explored_nodes:
                if not (self.error_flag and node == self.start_node):
                    continue
            constructed_path, costs = self.call_a_star(node)
            if constructed_path is not None:
                paths.append([constructed_path, costs, node])
            else:
                continue
            if self.current_target is node:
                self.old_cost = costs
                return [constructed_path]

        if len(paths) == 0 and len(to_explore) > 0:
            self.explored_nodes = set()
            return self.set_exploration_target()
        paths.sort(key=lambda x: x[1], reverse=False)
        path, cost, node = list(zip(*paths))[0], list(zip(*paths))[1][0], list(zip(*paths))[2][0]

        self.current_target = node
        self.old_cost = cost

        return path

    def draw(self):
        img = self.init_map()
        img = self.player.draw(img)
        img = self.lgraph.draw(img)
        img = self.rgraph.draw(img)
        if self.constructed_path is not None:
            for i in range(len(self.constructed_path) - 1):
                img = cv.line(img, self.constructed_path[i], self.constructed_path[i + 1], color=[0, 0, 0], thickness=2)
        if self.current_target is not None:
            img = cv.circle(img=img, center=self.current_target.pos, color=[0, 0, 0], thickness=-1, radius=2)
        img = cv.resize(img, (800, 800), interpolation=cv.INTER_LINEAR_EXACT)
        return img


def planning_control(remote_r, parent_remote_r, remote_a, parent_remote_a):
    parent_remote_r.close()
    planner = CPlan()
    path = [[]]
    time_start = time.time()
    plan_tick = 0
    while True:
        if remote_a.poll(0):
            name_r, (cmd, data) = remote_a.recv()
            if cmd == "target_reached":
                target_reached = True
                print('target_reached')
            elif cmd == "get_image":
                remote_a.send((name_r, planner.draw()))
            elif cmd == 'get_path_and_connections':
                remote_a.send((name_r, path))
            else:
                print(cmd, data)
                raise NotImplementedError

        remote_r.send(("slam_control", ("get_slam_data", None)))
        robot_data, (landmark_data, _) = remote_r.recv()
        theta, _ = robot_data['theta']

        # calculate new path
        path = planner.step(landmark_data, robot_data['position'], theta)
        '''if planner.reaction is not None:
            remote_r.send(("plan_execute_control", ("reaction", planner.reaction)))
        '''
        # get position again to update path (could have changed during planning)
        remote_r.send(("slam_control", ("get_slam_data", None)))
        robot_data, _ = remote_r.recv()
        player_position, _ = robot_data['position']
        path = planner.correct_path(path, player_position)
        # send it out
        if len(path) > 0:
            remote_r.send(("plan_execute_control", ("new_path", (path[0], planner.current_target.id, planner.racing))))

